package util

import (
	"fmt"
	"math/rand"
	"time"
)

// CompareSliceContents returns a bool indicating whether the supplied []int parameters contain equivalent values in equivalent order.
func CompareSliceOrder(first, second []int) bool {
	equal := true

	for i, v := range first {
		if second[i] != v {
			equal = false
			break
		}
	}

	return equal
}

// CompareSliceContents returns a bool indicating whether the supplied []int parameters contain equivalent values.
func CompareSliceContents(first, second []int) bool {
	if len(first) != len(second) {
		return false
	}

	equal := false

	firstMap := make(map[int]int)

	for _, i := range first {
		firstMap[i]++
	}

	for _, i := range second {
		_, ok := firstMap[i]
		if !ok {
			break
		}

		firstMap[i]--

		if firstMap[i] == 0 {
			delete(firstMap, i)
		}
	}

	if len(firstMap) == 0 {
		equal = true
	}

	return equal
}

// Make a slice containing pseudorandom numbers in [0, max).
func MakeRandomSlice(numItems, max int) []int {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	s := make([]int, numItems)
	for i := 0; i < numItems; i++ {
		s[i] = r.Intn(max)
	}

	return s
}

// Print at most numItems items.
func PrintSlice(slice []int, numItems int) {
	if numItems <= len(slice) {
		fmt.Println(slice)
		return
	}

	fmt.Println(slice[:numItems])
}

// Verify that the slice is sorted.
func CheckSorted(slice []int) bool {
	sorted := true

	for i := 0; i < len(slice)-1; i++ {
		if slice[i] > slice[i+1] {
			sorted = false
		}
	}

	return sorted
}
