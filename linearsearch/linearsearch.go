package linearsearch

func linearsearch(haystack []int, needle int) int {
	index := -1

	for i := 0; i < len(haystack); i++ {
		if haystack[i] == needle {
			index = i
		}
	}

	return index
}
