package linearsearch

import (
	"testing"
)

func TestLinearSearch(t *testing.T) {
	var tests = map[string]struct {
		haystack []int
		needle   int
		want     int
	}{
		"small haystack one": {
			haystack: []int{3, 4, 1, 6, 9, 7, 2, 5},
			needle:   9,
			want:     4,
		},
		"small haystack two": {
			haystack: []int{9, 3, 5, 2, 4, 1, 6, 7, 8},
			needle:   4,
			want:     4,
		},
		"medium haystack": {
			haystack: []int{34, 91, 82, 63, 0, 1, 2, 4, 8, 24, 25, 10, 61, 72, 89, 99, 64, 49, 26, 73, 51, 27, 58, 93, 26, 38, 46, 67, 10, 11, 18, 33, 68, 62, 54, 35, 17, 65, 84, 77},
			needle:   25,
			want:     10,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := linearsearch(tt.haystack, tt.needle)

			if got != tt.want {
				t.Errorf("want %v, got %v", tt.want, got)
			}
		})
	}

}
