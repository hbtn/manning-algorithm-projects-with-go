package countingsort

import (
	"testing"

	"gitlab.com/shodgesio/manning-algorithm-projects-with-go/util"
)

func TestCountingSort(t *testing.T) {
	var tests = map[string]struct {
		input []int
		want  []int
	}{
		"small input one": {
			input: []int{3, 6, 1, 6, 9, 7, 1, 5},
			want:  []int{1, 1, 3, 5, 6, 6, 7, 9},
		},
		"small input two": {
			input: []int{9, 3, 5, 2, 4, 1, 6, 7, 8},
			want:  []int{1, 2, 3, 4, 5, 6, 7, 8, 9},
		},
		"medium input one": {
			input: []int{3, 9, 8, 6, 0, 1, 2, 4, 8, 2, 0, 2, 1, 0, 6, 7, 8, 9, 6, 4, 2, 7, 5, 2, 5, 9, 2, 3, 4, 6, 0, 3, 6, 6, 5, 3, 0, 6, 8, 7},
			want:  []int{0, 0, 0, 0, 0, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9},
		},
		"medium input two": {
			input: []int{8, 5, 3, 0, 8, 0, 7, 8, 7, 4, 6, 0, 5, 5, 2, 5, 0, 7, 3, 3, 8, 7, 6, 5, 3, 0, 8, 5, 7, 7, 2, 3, 0, 5, 8, 5, 5, 2, 5, 6},
			want:  []int{0, 0, 0, 0, 0, 0, 2, 2, 2, 3, 3, 3, 3, 3, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8, 8, 8},
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := countingsort(tt.input)

			if !util.CompareSliceOrder(tt.want, got) {
				t.Errorf("want %v, got %v", tt.want, got)
			}

		})
	}

}
