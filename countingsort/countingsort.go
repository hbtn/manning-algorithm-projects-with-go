package countingsort

func countingsort(slice []int) []int {
	hi := 0

	for i := 0; i < len(slice); i++ {
		if slice[i] > hi {
			hi = slice[i] + 1
		}
	}

	b := make([]int, len(slice))
	c := make([]int, hi)

	for i := 0; i < len(slice); i++ {
		c[slice[i]]++
	}

	for i := 1; i < len(c); i++ {
		c[i] += c[i-1]
	}

	for i := len(slice) - 1; i >= 0; i-- {
		c[slice[i]]--
		b[c[slice[i]]] = slice[i]
	}

	return b
}
