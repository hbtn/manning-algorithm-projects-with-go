package bubblesort

func bubbleSort(slice []int) {
	for {
		swapped := false

		for i := 0; i < len(slice)-1; i++ {
			if slice[i] > slice[i+1] {
				slice[i], slice[i+1] = slice[i+1], slice[i]
				swapped = true
			}
		}

		if !swapped {
			break
		}
	}
}
