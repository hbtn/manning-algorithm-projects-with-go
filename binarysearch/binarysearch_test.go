package binarysearch

import (
	"testing"
)

func TestBinarySearch(t *testing.T) {
	var tests = map[string]struct {
		haystack []int
		needle   int
		want     int
	}{
		"small haystack one": {
			haystack: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			needle:   9,
			want:     8,
		},
		"small haystack two": {
			haystack: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			needle:   4,
			want:     3,
		},
		"medium haystack one": {
			haystack: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50},
			needle:   25,
			want:     24,
		},
		"medium haystack two": {
			haystack: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50},
			needle:   46,
			want:     45,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got := binarysearch(tt.haystack, tt.needle)

			if got != tt.want {
				t.Errorf("want %v, got %v", tt.want, got)
			}
		})
	}

}
