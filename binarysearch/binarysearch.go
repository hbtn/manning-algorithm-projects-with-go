package binarysearch

import "math"

func binarysearch(haystack []int, needle int) int {
	index := -1

	lo := 0
	hi := len(haystack)

F:
	for lo < hi {
		m := int(math.Floor(float64(lo) + (float64(hi)-float64(lo))/2))

		switch {
		case haystack[m] == needle:
			index = m
			break F
		case haystack[m] > needle:
			hi = m
			break
		case haystack[m] < needle:
			lo = m + 1
			break
		}
	}

	return index
}
