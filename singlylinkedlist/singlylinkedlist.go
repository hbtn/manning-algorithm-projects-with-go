package singlylinkedlist

type Cell struct {
	data string
	next *Cell
}
