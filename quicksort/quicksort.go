package quicksort

func partition(slice []int) int {
	pivot := len(slice) - 1
	idx := -1

	for i := 0; i < pivot; i++ {
		if slice[i] < slice[pivot] {
			idx++
			slice[idx], slice[i] = slice[i], slice[idx]
		}
	}

	idx++
	slice[pivot], slice[idx] = slice[idx], slice[pivot]
	return idx
}

func quicksort(slice []int) {
	if len(slice) <= 1 {
		return
	}
	pivot := partition(slice)

	quicksort(slice[:pivot])
	quicksort(slice[pivot+1:])
}
